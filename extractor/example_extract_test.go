/*
    extract_syntactic: extract information included in the file
    It's based strongly in the syntax of the file.

    Copyright (C) 2014 Xavier <quatrilio@gmail.com> and other authors.
    See CONTRIBUTORS file.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Website: https://github.com/quatrilio/propref

*/

/*
  Test and examples of extract library. From https://godoc.org/github.com/natefinch/godocgo
*/
package extractor_test


import (
        "fmt"
        "log"

        "github.com/quatrilio/propref/extractor"
)

// Example of the Mathematical Subject Classification struct. We put information contained in one document to three MSC structs.
func ExampleMSC() {

	//The string of Mathematical Subject Classification in the document ArXiV:1401.5978v1 is
	//{\it 2000 Mathematics Subject Classifications}: Primary 11B65, Secondary 05A10, 05A30
	//We put this information in three different MSC structs
  
	// Primary 11B65
	msc1:= MSC{}
	msc1.Version = 2010
	msc1.Type = "primary"
	msc1.Code = "11B65"

	// Secondary 05A10
	msc2:= MSC{}
	msc2.Version = 2010
	msc2.Type = "secondary"
	msc2.Code = "05A10"

	// Secondary 05A30
	msc3:= MSC{}
	msc3.Version = 2010
	msc3.Type = "secondary"
	msc3.Code = "05A30"

}

// Example of the Statement struct. We put the information of Pythagorean Theorem 
func ExampleStatement() {

	//If we have this: "Theorem of Pythagoras: In a triangle with right angle,
	// the square of the hypotenuse is equal to the sum of the squares of the other two sides"
  
	argument := Argument{}
	argument.Hipothesis = "In a triangle with right angle"
	argument.Thesis = "The square of the hypotenuse is equal to the sum of the squares of the other two sides"
  
	st := Statement{}
	st.Type="theorem"
	st.Argument = argument
	st.Assertion = "In a triangle with right angle, the square of the hypotenuse is equal [...] sides"
	// We omit the whole assertion for better view.
 	t.Name = "of Pythagoras"

}

