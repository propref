/*
    extract_syntactic: extract information included in the file
    It's based strongly in the syntax of the file.

    Copyright (C) 2014 Xavier <quatrilio@gmail.com> and other authors.
    See CONTRIBUTORS file.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Website: https://github.com/quatrilio/propref

*/

/*

  extractor is a library for extracting information about mathematical documents.
  The information could be: a) syntactic: within the document, like title. b) Meta-informational: typically filesystem information, like size or output from `file` command. c) Contextual: depending of whole documents, as a network [1]. Like the impact index of the document or the popularity of the propositions.
    
  [1] "The information provided by a network itself is different than the sum of the information provided by each individual element in the network" is one of the conclusions from reading the book "Redes Complejas" of Ricard Solé (spanish).

*/
package extractor

import (
	"regexp"
	"log"
	"errors"
	"github.com/quatrilio/propref/tools"
)

// List of strings we search for in the source documents, by syntax
// BUG(#3): latex commands could have space, return carriages inside .... But only just one return carriage.
// BUG(#5): it does not work with arxiv:1401.5978v1
// BUG(#4): LaTeX: \documentclass{amsart} and others incorporate amsthm (See http://www.ctan.org/pkg/amsart)
var regs map[string]string = map[string]string {
	"latex": `(\\usepackage(?:\[\w*(?:,\w*)?\])??{(?:\w*,)?(?P<usepkg_amsmath>amsmath)(?:,\w*)?})`, // for LaTeX
}

// SInfo represents syntactic information of the document. It consists of a common fields of mathematical documents.
type SInfo struct {

	Doctype string //Type of the document (latex, context, etc.). Always is lowercase
	Subdoctype string //Subtype of the document (for example, amslatex, plain, amsart)
	Title string //Title of the document
	Authors []Author //list of authors of the document, ordered by their position in the text
	Date string //when the document was written. In format "YYYY-MM-DD" (Gregorian calendar)
	Resume string //the abstract
	License string //the license of the document
	MSC []MSC //Mathematical Subject Classification list items
	Keywords []string //Most important words of the document
	Words []string //Set (not list) of all words
	Language string //The language of the document
	References []Reference //References appearing in the bibliography
	Statements []Statement //List of all statements
}

// Author is related to author information. By now, it's just a string, with his or her complete name (including alias if any).
type Author struct {

	Name string //Whole name of the author
}

// MSC represents an Mathematical Subject Classification item.
type MSC struct {

	Version uint // the year of revision of the MSC which is used in the document. Eg. 2010
	Type string // "primary" or "secondary".
	Code string // code. eg "05A10"
}

// Reference implements information of a reference. A reference is basically an information of document which has more information about whatever we said.
type Reference struct {

	Author []Author //the authors of the document
	Title string //the title of the document
	Date string //date which is published or created
	URL string //the URL in which we found
		
}

// Statement implements units of information in mathematical documents: concluding items (like theorems, lemmas, corollaries, etc.) in which we arrive at conclusions about objects, a premise item (like definitions, notation, axioms, hyphotesis, conditions) in which we what we need before reasoning, or a remark item (conjecture, problem, note) in which we just note or remark.
type Statement struct {

	Type string //type of the statement
	Argument Argument //for propositions only
	Assertion string //what is said
	Name string //things could have optionally names
	References []Reference //if the statements points to a references for better understanding
	Documents []Reference //list of documents in which the statement is present
	
}

// Argument is composed by the hypothesis and the thesis, which are parts of one proposition.
type Argument struct {
	Hipothesis string
	Thesis string	
}

// Extract_Latex extracts SInfo from LaTeX source files.
func Extract_Latex(contents string) (*SInfo, error) {

	// Compile and search regular expressions depending on syntax
	re, err := regexp.Compile(regs["latex"])

	if err != nil {
		log.Fatal(err)
		return nil, errors.New("[PropRef:extract_syntactic:Extract_Latex] " + err.Error())
	}
	
	result := SInfo{}
	// Extract all the substrings from contents
	a := tools.FindStringSubmatchMap(re, contents)
	result.Doctype = "latex"
	result.Subdoctype = a["usepkg_amsmath"]
	return &result, nil


}

// Extract_Syntax extracts information from the string
// according to the specified syntax.
// It retuns a SInfo struct.
func Extract_Syntax(contents string, syntax string) (*SInfo, error) {

	switch syntax {
		case "latex": return Extract_Latex(contents)
		default: return nil, errors.New("[PropRef:extract_syntactic] Not suported syntax")
	}


}


