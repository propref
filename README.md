# PropRef. Propositions' references #

The aim of this project is to provide a search engine for (mathematical) axioms, definitions, theorems, lemmas, corollaries, etc. Not mere search engine of documents containing this elements, but a search engine of the results and axioms of Axiomatic Theories. An example: I want to know a new area of mathematics: triangular conorms.

  *  What are the main theorems of this area?
  *  What are the definitions I have to learn?
  *  What are the open problems?
  *  Can I get the demostrations of the main results?
  *  What sources I have to read first to understand this area?
  *  What are the most cited articles?
  *  Are there a books of this area?
  *  In what Axiomatic Theory does it belong to?
  *  What are the main authors in this area?
  *  And what are the journals of reference?

`PropRef` is created for *trying* to solving these problems.

## Source code ##

  * You can obtain the source code running:
    * `git clone https://github.com/quatrilio/propref.git` or
    * `git clone http://repo.or.cz/w/propref.git`
  * If you want to obtain the source code of the wiki, you can run:
    * `git clone https://github.com/quatrilio/propref.wiki.git` or
    * `git clone http://repo.or.cz/w/propref-wiki.git`

## Installation ##

### Dependencies ###

  * Install `git` on your system
  * [Set properly]((http://golang.org/doc/code.html#GOPATH)) `GOPATH` and `GOROOT`

### Installation ###

You have to run

```
go get github.com/quatrilio/propref
```

and it will install the binary `propref` into `$GOPATH/bin`.

If you want to update the package, just run

```
go get -u github.com/quatrilio/propref
```

## Usage ##

`propref` or `propref --help` prints help about usage of the program:

```
Usage of propref:
  -path="": Path of the file. Example /home/user/foo.tex
  -syntax="auto": Syntax of the file: latex, context, ... If 'auto' the syntax is determining with the type of the file (according to file command)
```

I think it's self-explaning thing.

## Documentation ##

You can see all the documentation in the [wiki](https://github.com/quatrilio/propref/wiki) (or [here](http://repo.or.cz/w/propref-wiki.git)).

## License ##

This repository contains three parts:

  * The software which basically extracts information of several files. This information is mainly formed by propositions, definitions, axioms and any other textual information such as language, authors of paper, etc.
  * The database of this extracted information
  * And the documentation of the software, contents of the web page or any other things related.

The software is distributed under de the [Affero General Public License 3.0](http://www.gnu.org/licenses/agpl-3.0.html) or above, the database is distributed under the [Open Database License 1.0](http://opendatacommons.org/licenses/odbl/) or above and the documentation is released under the [Creative Commons Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) or above.

All the contents of the repository are property of the respective owners: Xavier <quatrilio@gmail.com> and other contributions. All the contribution can be seen in the history of the repository.

## Participation ##

The participation of this project is open. If you want to contribute to code, documentate or anything else, contact me. I will be very glad to see participation. If your contribution is worthy, I will integrate to this project. If you don't like my criteria, you could fork this project. No problem.

