/*
    toolset: Several useful functions
    Copyright (C) 2014 Xavier <quatrilio@gmail.com> and other authors.
    See CONTRIBUTORS file.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Website: https://github.com/quatrilio/propref

*/

/*
  tools library implements a set of shortcuts for speeding things.

*/
package tools

import (
	"os"
	"regexp"
	"io/ioutil"
	"errors"
	"log"
	iconv "github.com/djimenez/iconv-go"
	
)


// Exists checks whether the file or a directory does exist or not
// It returns true if file or directory exists and false otherwise
func Exists(path string) bool {

	_, err := os.Stat(path)
	
	if err == nil {
		return true
	}
	
	if os.IsNotExist(err) {
		return false
	}

	return false

}

// IsFile checks whether a path is a file or not
// It returns true in case of path is file and false otherwise.
func IsFile(path string) bool {

	f, err := os.Stat(path)
	
	if err == nil {
		if f.Mode().IsRegular() {
			return true
		} else {
			return false
		}
	}
	
	return false

}


// FinsStringSubmatchMap return a map consisting of: named fields of r as the keys of the map and the matching subregular expression as the values. 
// Adapting from http://play.golang.org/p/2E4aajU4go
func FindStringSubmatchMap(r *regexp.Regexp, s string) map[string]string {
	captures := make(map[string]string)

	match := r.FindStringSubmatch(s)
	if match == nil {
		return captures
	}

	for i, name := range r.SubexpNames() {
		// 
		if i == 0 {
			continue
		}
		captures[name] = match[i]

	}
	return captures
}


// ReadContentUFT8 reads the contents from the file and pass these contents to UTF-8 encoding.
func ReadContentUTF8(path string, encoding string) (contents string, err error) {

	// Read the contents of the file, whatever encoding it has
	dat, err := ioutil.ReadFile(path)
	if err!=nil {
		log.Fatalf("[PropRef:toolset] error reading file contents: %v", err)
		return "", errors.New("[PropRef:tools] Error reading file " + err.Error())
	}
		
	// We need to convert that to UTF-8 if the encoding is different to UTF-8
	data, err := iconv.ConvertString(string(dat), encoding, "utf-8")
	if err != nil {
		log.Fatalf("[PropRef:tools] error converting file %v with encoding %v to UTF-8: %v.", path, encoding, err)
		return "", errors.New("[PropRef:tools] Error converting file encoding " + err.Error())
	}
	return data, nil

}