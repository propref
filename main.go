/*
    main.go: The PropRef main program.
    Copyright (C) 2014 Xavier <quatrilio@gmail.com> and other authors.
    See CONTRIBUTORS file.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Website: https://github.com/quatrilio/propref

*/

/*
    The PropRef implements a searching engine for mathematical propositions and information about
    mathematical documents.
*/
package main

import (
	"flag"
	"log"
	"fmt"
	"github.com/quatrilio/propref/extractor"
	"github.com/quatrilio/propref/tools"
)

const (

	propref_version = "0.007-dev"
)


// The main procedure. It extracts information from one file
func main() {

	path := flag.String("path", "", "Path of the file. Example /home/user/foo.tex")
	syntax := flag.String("syntax", "latex", "Syntax of the file: latex, context, ...")
	encoding := flag.String("encoding", "utf-8", "Encoding of the file. Eg. iso-8859-1. Always in lowecase.")
	version := flag.Bool("version", false, "Print the version")

	flag.Parse()

	if *version {
		fmt.Printf("PropRef - Propositions' References %v\n", propref_version)
	} else {

		if *path == "" {
			fmt.Printf("Empty path. See propref --help for reference\n")
		} else {
	
			if tools.Exists(*path) {
		
				if tools.IsFile(*path) {

					//Read the contents from the file and pass to UTF-8 encoding
					contents, err := tools.ReadContentUTF8(*path, *encoding)
					if err != nil {
						log.Fatalf("Error while extracting information of %v: %v\n", *path, err)
					}

					// Extract information according to the syntax
					result, err := extractor.Extract_Syntax(contents, *syntax)
					if err != nil {
						log.Fatalf("Error while extracting information of %v: %v\n", *path, err)
					} else {
				
						fmt.Printf("%+v\n",*result)
					}
				
				} else {
			
					fmt.Printf("The path %v is not a file\n", *path)
			
				}
			
			} else {

				fmt.Printf("The path %v does not exists\n", *path)
		
			}

		}
	}
	
}
